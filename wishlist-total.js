function translation (language) {
  const mapping = {
    de: { subtotal: 'Zwischensumme' },
    cs: { subtotal: 'Mezisoučet' },
    es: { subtotal: 'Subtotal' },
    it: { subtotal: 'Totale parziale' },
    nl: { subtotal: 'Subtotaal' },
    pl: { subtotal: 'Suma częściowa' },
    tr: { subtotal: 'Ara toplam' },
    fr: { subtotal: 'Total' },
    sv: { subtotal: 'Delsumma' },
    cn: { subtotal: '小计' },
    zh: { subtotal: '小计' },
    eg: { subtotal: 'المجموع الفرع' },
    hi: { subtotal: 'उप-योग' },
    ta: { subtotal: 'கூட்டுத்தொகை' },
    te: { subtotal: 'ఉపమొత్తం' },
    kn: { subtotal: 'ಉಪಮೊತ್ತ' },
    ml: { subtotal: 'മൊത്തം' },
    bn: { subtotal: 'সাবটোটাল' },
    mr: { subtotal: 'एकूण' },
    jp: { subtotal: '小計' },
    ar: { subtotal: 'المجموع الفرعي' }
  }

  return mapping[language] || { subtotal: 'Subtotal' }
}

const items = document.getElementsByClassName('g-item-sortable')

if (items.length > 1) {
  const locale = document.querySelector('html[lang]').getAttribute('lang') || window.navigator.languages[0] || window.navigator.language
  const language = locale.split('-')[0]
  const translations = translation(language)

  const prices = Array.prototype.map.call(items, (item) => {
    const price = item.getAttribute('data-price')
    return parseFloat(price === '-Infinity' ? 0 : price)
  })

  const total = prices.reduce((item, sum) => (sum += item))
  const prettyTotal = total.toLocaleString(
    locale,
    { style: 'currency', currency: 'USD' }
  ).replace(/USD|US|\$/g, '')

  const wishlistTotalContainer = document.createElement('div')
  wishlistTotalContainer.style.padding = '10px 0 10px 0'
  wishlistTotalContainer.style.textAlign = 'center'
  wishlistTotalContainer.style.fontSize = '1.5rem'
  wishlistTotalContainer.className = 'a-row'
  wishlistTotalContainer.id = 'wishlist-total-container'
  wishlistTotalContainer.innerText = `${translations.subtotal} (${prices.length}): ${prettyTotal}`

  document.getElementById('wl-item-view')
    .prepend(wishlistTotalContainer)
}
